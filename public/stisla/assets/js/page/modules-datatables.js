("use strict");

$.ajaxSetup({
  headers: {
    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
  },
});

$("#table-1").dataTable({
  processing: true,
  serverSide: true,
  pageLength: 5,
  lengthMenu: [
    [5, 10, 25, 50, 100],
    [5, 10, 25, 50, 100],
  ],
  bFilter: true,
  bInfo: true,
  processing: true,
  bServerSide: true,
  order: [[1, "asc"]],
  ajax: {
    url: "/datamenu",
    headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
    type: "POST",
    data: function (d) {
      // console.log(d);
      // headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") };
      _token: $('meta[name="csrf-token"]').attr("content");
    },
  },
  columns: [
    {
      render: function (data, type, row, meta) {
        return row.nama_menu;
      },
    },
    {
      render: function (data, type, row, meta) {
        return row.status_aktif;
      },
    },

    // test
    {
      data: null,
      orderable: false,
      render: function (data, type, row) {
        return `<a id="test" href='#' class='btn btn-sm btn-info' onClick='edit(${row.id})'>Edit</a> <a href='#' id='testalert' onClick='deletedata(${row.id})' class='btn btn-sm btn-danger'>Hapus</a> `;
      },
    },

    // test
  ],
});
