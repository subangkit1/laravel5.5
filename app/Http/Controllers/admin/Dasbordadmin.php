<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\menu\Menu;
class Dasbordadmin extends Controller
{
    public function index(){
        $data = Menu::select('*')->get();
        // dd($data);
    }
    public function datamenu(){
        // dd("test");
        $columns = [
            'nama_menu',
            'status_aktif',
        ];
        $orderBy = $columns[request()->input("order.0.column")];
        $data = Menu::select('*');

        // fungsi search pojok atas 
        if(request()->input("search.value")){
            // disini jika ditambahkan semua 
            $data = $data->where(function($query){
                    $query->whereRaw('LOWER(nama_menu) like ?',['%'.strtolower(request()->input("search.value")).'%'])
                    ->orWhereRaw('LOWER(status_aktif) like ?',['%'.strtolower(request()->input("search.value")).'%']);
            });
        }
        $recordsFiltered = $data->get()->count();
        // limit / paginationnya 
        $data = $data->skip(request()->input('start'))->take(request()->input('length'))->orderBy($orderBy,
        request()->input("order.0.dir"))->get();
        $recordsTotal = $data->count();
        // dd(request()->all());
        return response()->json([
            'draw' => request()->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data,
            'all_request' => request()->all()
        ]);
   
    }
}
