<?php

namespace App\Http\Controllers;
use App\menu\Menu;
use App\menu\Submenu;
use Illuminate\Http\Request;

class Menucontroller extends Controller
{
    

    public function index(){
        // dd("test");
        $menu = Menu::all();
        return view('test',compact('menu'));

    }

    public function disablemenu(Request $request){
        $id = request()->id;
        $datamenu = Menu::find($id);
        $datamenu->status_aktif = 1;
        $status = $datamenu->save();
        if($status){
            return response()->json([
                'keterangan' => 'tersimpan',
                'status'=> 200
            ]);
        }else{
            return response()->json([
                'keterangan' => 'gagal',
                'status' => 500
            ]);
        }
    }

    public function edit($id){
        $data = Menu::find($id);
        return response()->json($data);
        
    }

    public function updatedata(){
        //validasi inputan 
        $updatedata = Menu::find(request()->id);
        $updatedata->nama_menu = request()->namamenu;
        $updatedata->icon = request()->iconmenu;
        $status = $updatedata->save();
        if($status){
            return response()->json([
                'keterangan' => 'berhasil di update',
                'status' => 200
            ]);
        }else{
            return response()->json([
                'keterangan' => 'gagal di update',
                'status' => 500
            ]);
        }

        
    }
}
