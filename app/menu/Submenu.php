<?php

namespace App\menu;

use Illuminate\Database\Eloquent\Model;

class Submenu extends Model
{
    protected $table = 'submenus';
    protected $fillable = ['menu_id','namasub_menu','nama_route','url_link','prioritas','status_aktif'];
    protected $timestamp = true;

    public function menus(){
        return $this->belongsTo('\App\menu\Menu','menu_id','menu_id');
    }
}
