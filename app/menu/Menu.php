<?php

namespace App\menu;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //

    protected $table = 'menus';
    protected $fillable = ['menu_id','nama_menu','prioritas','icon','status_aktif'];
    protected $timestamp = true;

    public function submenu()
    {
        return $this->hasMany('App\menu\submenu','menu_id','menu_id');

    }
    
}
