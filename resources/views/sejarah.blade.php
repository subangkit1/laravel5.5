<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Visi dan Misi</title>
      <!-- Link ke Bootstrap CSS -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" />
      <!-- Link ke Bootstrap Icons CSS -->
      {{-- 
      <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.17.0/font/bootstrap-icons.css" rel="stylesheet" />
      --}}
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css" integrity="sha384-b6lVK+yci+bfDmaY1u0zE8YYJt0TZxLEAFyYSLHId4xoVvsrQu3INevFKo+Xir8e" crossorigin="anonymous" />
      <!-- Option 1: Include in HTML -->
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
      <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" />
      <style>
         <!-- CSS Khusus Untuk Navbar Sticky -->
      <style>
         /* Gaya Navbar Sticky */
         .sticky-top {
         position: sticky;
         top: 0;
         z-index: 1000;
         }
         /* Gaya Carousel */
         .carousel-item {
         height: auto;
         }
         /* Gaya Card */
         .custom-card {
         background-color: #0097a7;
         color: #fff;
         }
         /* Gaya Tombol 3D */
         .btn-3d {
         background-color: #0097a7;
         color: #fff;
         text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.2);
         border: none;
         border-radius: 8px;
         transition: transform 0.2s;
         }
         .btn-3d:hover {
         transform: scale(1.05);
         }
         .custom-bg {
         background-color: #0097a7; /* Ganti dengan warna yang Anda inginkan */
         color: #fff; /* Warna teks */
         }
         .kemenkes-bg {
         background-color: #b2bec3; /* Warna biru tua */
         color: white; /* Warna teks putih */
         }
         .accordion-button:not(.collapsed) {
         background-color: #0097a7; /* Warna biru tua ketika terbuka */
         color:white
         }
         /* Responsif untuk Tampilan Mobile */
         @media (max-width: 767px) {
         .navbar-nav {
         flex-direction: column;
         text-align: center;
         }
         }
      </style>
   </head>
   <body style="background-color: #f5f5f5; font-family: verdana;">
      <!-- Header Informasi Website -->
      <div class="container-fluid custom-card">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="text-white text-left py-2">
                     <p><img src="https://bpfkmakassar.go.id/images/logo-mobile.png" /></p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
         <div class="container">
            {{-- <a class="navbar-brand" href="#">Logo</a> --}}
            <!-- Tombol Toggle untuk Tampilan Mobile -->
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <!-- Daftar Menu -->
            <div class="collapse navbar-collapse" id="navbarNav">
               <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                     <a class="nav-link" href="#"><i class="bi bi-house-door"></i> Beranda</a>
                  </li>
                  <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bi bi-person"></i> Profil </a>
                     <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                        <a class="dropdown-item" href="#">Visi & Misi</a>
                        <a class="dropdown-item" href="#">Sejarah</a>
                        <a class="dropdown-item" href="#">Struktur Organisasi</a>
                        <a class="dropdown-item" href="#">Kontak</a>
                     </div>
                  </li>
                  <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bi bi-person"></i> Event </a>
                     <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                        <a class="dropdown-item" href="#">Submenu 1</a>
                        <a class="dropdown-item" href="#">Submenu 2</a>
                     </div>
                  </li>
                  <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bi bi-person"></i> Informasi Publik </a>
                     <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                        <a class="dropdown-item" href="#">Submenu 1</a>
                        <a class="dropdown-item" href="#">Submenu 2</a>
                     </div>
                  </li>
                  <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bi bi-person"></i> Unduh </a>
                     <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                        <a class="dropdown-item" href="#">Submenu 1</a>
                        <a class="dropdown-item" href="#">Submenu 2</a>
                     </div>
                  </li>
                  <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bi bi-person"></i> Layanan </a>
                     <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                        <a class="dropdown-item" href="#">Submenu 1</a>
                        <a class="dropdown-item" href="#">Submenu 2</a>
                     </div>
                  </li>
                  <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bi bi-gear"></i> Pengaturan </a>
                     <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                        <a class="dropdown-item" href="#">Submenu 1</a>
                        <a class="dropdown-item" href="#">Submenu 2</a>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
      <!-- Slider di Bawah Navbar -->

      <div class="container">
         <div class="row">
            <div class="col-md-12 mt-3 mb-3">
               <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='currentColor'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                     <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Sejarah </li>
                     </ol>
               </nav>
            </div>
         </div>


         {{-- konten body --}}
         <div class="row mb-5">
            <div class="col-md-12">
               <div class="card" style="border:none">
                 <div class="card-body">
                  <p class="h5">SEJARAH</p>
                  <P style="text-align:justify">Bermula dari dibentuknya pelayanan dosis radiasi perorangan di RS Dr Soetomo Surabaya. Kemudian pada tahun 1985 dimulai dengan pembentukan Balai Pemeliharaan Peralatan, Proteksi Radiasi dan Kalibrasi (BP3K). Dengan berdirinya BP3K laboratorium radiasi yang berada di RS Dr Soetomo  dipindahkan ke Jl Indrapura Sidoluhur No.17 Surabaya. Pada tahun 1992 melalui Peraturan Menteri Kesehatan No. 282 Tahun 1992 BP3K berubah menjadi Balai Pengamanan Fasilitas Kesehatan (BPFK) tipe B. Mulai tahun 2000 dengan Peraturan Menkes No. 1164 Tahun 2000 BPFK Surabaya naik dari Tipe B menjadi BPFK Tipe A yang setara eselon IIIa dengan penambahan seksi Kemitraan dan Bimbingan Teknik.</P>

                <p style="text-align:justify">Surat Menteri Kesehatan bulan Agustus tahun 2001 menetapkan bahwa Ex Kantor Kanwil yang berkedudukan di Jalan Karang Menjangan No.22 Surabaya diperuntukkan untuk BPFK Surabaya. Dengan dana yang terbatas dari dinas, tahun 2002 berhasil merenovasi satu Laboratorium Proteksi Radiasi diikuti renovasi pembangunan dan renovasi secara bertahap. Hingga pada Tahun 2005, BPFK Surabaya yang semula berkedudukan di Jalan Sidoluhur No.17 hijrah ke Jalan Karangmenjangan No.22 Surabaya.</p>

                <p style="text-align:justify">Pada tahun 2007, sesuai Permenkes No. 530/Menkes/Per/IV/2007 maka BPFK Surabaya menjadi salah satu Unit Pelaksana Pusat Sarana, Prasarana dan Peralatan Kesehatan ( PSPPK ). Struktur organisasi BPFK Surabaya selanjutnya mengalami perubahan sesuai agenda reformasi birokrasi dengan adanya peraturan yang terbaru yaitu berdasarkan Permenkes No. 61 Tahun 2020 tentang Organisasi dan Tata Kerja Unit Pelaksana Teknis Bidang Pengamanan Fasilitas Kesehatan.</p>

                <p style="text-align:justify">
                    Saat ini jangkauan pelayanan BPFK Surabaya meliputi 8 provinsi yaitu Jawa Timur, Bali, Nusa Tenggara Barat, Nusa Tenggara Timur, Kalimantan Selatan, Kalimantan Tengah, Kalimantan Utara dan Kalimantan Timur.
                </p>

                <img src="https://bpfk-sby.org/public/konten/uploads/medium/medium_gedung_bpfk.jpg" class="img-fluid" alt="...">
                 </div>
               </div>
            </div>
         </div>

         {{-- end konten body --}}

         
      </div>
      {{-- brudcrumb --}}
      
      {{-- end brundrumb --}}
     
      {{-- container kedua --}}
 

   


      {{-- keunggulan  --}}



      {{-- end keuanggulan --}}
      {{-- footer --}}
      <footer class="bg-white w-100" style="position:none;bottom:0;">
        <div class="container py-4">
            <div class="row">
                <div class="col-md-6">
                    <h6>Footer Content</h6>
                    <p>Tambahkan konten footer Anda di sini.</p>
                </div>
                <div class="col-md-6">
                    <h6>Contact Information</h6>
                    <address>
                        Alamat Anda<br>
                        Kota, Negara<br>
                        Email: email@example.com<br>
                        Telepon: +123456789
                    </address>
                </div>
            </div>
        </div>
    </footer>
      {{-- end footer --}}


      <!-- Tiga Kolom dengan Pewarnaan Kemenkes -->
      <!-- Link ke Bootstrap JavaScript (Popper.js dan Bootstrap.js) -->
      <script
         src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
         integrity="sha512-2rNj2KJ+D8s1ceNasTIex6z4HWyOnEYLVC3FigGOmyQCZc2eBXKgOxQmo3oKLHyfcj53uz4QMsRCWNbLd32Q1g=="
         crossorigin="anonymous"
         referrerpolicy="no-referrer"
         ></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js"></script>
   </body>
</html>