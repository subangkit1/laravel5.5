<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>Dashboard admin</title>
      <!-- General CSS Files -->
      <link rel="stylesheet" href="{{ asset('stisla/assets/modules/bootstrap/css/bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('stisla/assets/modules/fontawesome/css/all.min.css') }}">
      <!-- CSS Libraries -->
      {{-- data tables --}}
      <link rel="stylesheet" href="{{ asset('stisla/assets/modules/datatables/datatables.min.css') }}">
      <link rel="stylesheet" href="{{ asset('stisla/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
      <link rel="stylesheet" href="{{ asset('stisla/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
      {{-- end data tables --}}
      <!-- Template CSS -->
      <link rel="stylesheet" href="{{ asset('stisla/assets/css/style.css') }}">
      <link rel="stylesheet" href="{{ asset('stisla/assets/css/components.css') }}">
      <style type="text/css">
         .dataTables_length{
         margin-right:10px
         }
      </style>
      <!-- Start GA -->
      {{-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script> --}}
      <script>
         //   window.dataLayer = window.dataLayer || [];
         //   function gtag(){dataLayer.push(arguments);}
         //   gtag('js', new Date());
         
         //   gtag('config', 'UA-94034622-3');
      </script>
      <!-- /END GA -->
   </head>
   <body>
      <div id="app">
         <div class="main-wrapper main-wrapper-1">
            <div class="navbar-bg"></div>
            <nav class="navbar navbar-expand-lg main-navbar">
               <form class="form-inline mr-auto">
                  <ul class="navbar-nav mr-3">
                     <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
                     <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
                  </ul>
               </form>
               <ul class="navbar-nav navbar-right">
                  <li class="dropdown">
                     <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                        <img alt="image" src="" class="rounded-circle mr-1">
                        <div class="d-sm-none d-lg-inline-block">Hi, Ujang Maman</div>
                     </a>
                     <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-title">Logged in 5 min ago</div>
                        <a href="features-settings.html" class="dropdown-item has-icon">
                        <i class="fas fa-cog"></i> Settings
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item has-icon text-danger">
                        <i class="fas fa-sign-out-alt"></i> Logout
                        </a>
                     </div>
                  </li>
               </ul>
            </nav>
            <div class="main-sidebar sidebar-style-2">
               <aside id="sidebar-wrapper">
                  <div class="sidebar-brand">
                     <a href="index.html">BPFK Blog</a> 
                    </div>
                  <div class="sidebar-brand sidebar-brand-sm">
                     <a href="index.html">St</a>
                  </div>
                  <ul class="sidebar-menu">
                     <li class="menu-header">Dashboard</li>
                     <li class="dropdown">
                        <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                        <ul class="dropdown-menu">
                           <li><a class="nav-link" href="index-0.html">General Dashboard</a></li>
                           <li><a class="nav-link" href="index.html">Ecommerce Dashboard</a></li>
                        </ul>
                     </li>
                  </ul>
               </aside>
            </div>
            <!-- Main Content -->
            <div class="main-content">
               {{-- @yield tapi ini hanya contoh  --}}
               <section class="section">
                  <div class="section-header">
                     <h1>Blank Page</h1>
                     {{-- <button class="btn btn-primary trigger--fire-modal-4" id="modal-4">Footer Background</button> --}}
                  </div>
                  <div class="section-body">
                     {{-- isi disini  --}}
                     <div class="row">
                        <div class="col-12">
                           <div class="card">
                              <div class="card-body">
                                 <div class="table-responsive">
                                    <table class="table table-striped" id="table-1">
                                       <thead>
                                          <tr>
                                             <th>Nama Menu</th>
                                             <th>Statu Menu</th>
                                             <th></th>
                                             {{-- 
                                             <th></th>
                                             --}}
                                          </tr>
                                       </thead>
                                       <tbody>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     {{-- end isi disini --}}
                  </div>
               </section>
               {{-- open modal --}}
               <section>
                  <div class="modal fade" tabindex="-1" role="dialog" id="fire-modal-1" aria-hidden="true" style="display: none;">
                     <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">
                           <div class="modal-header">
                              <h5 class="modal-title">Form Ubah Data</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>           
                           </div>
                           <div class="modal-body">
                            
                            <span id="datamenu"></span>
                            
                            
                          </div>
                        </div>
                     </div>
                  </div>
               </section>
               {{-- end modal  --}}
            </div>
            {{-- end main konten --}}
            <footer class="main-footer">
               <div class="footer-left">
                  Copyright &copy; 2018 
                  <div class="bullet"></div>
                  Design By <a href="">Muhamad Nauval Azhar</a>
               </div>
               <div class="footer-right">
               </div>
            </footer>
         </div>
      </div>
      <!-- General JS Scripts -->
      <script src="{{ url('stisla/assets/modules/jquery.min.js') }}"></script>
      <script src="https://unpkg.com/@popperjs/core@2.11.8/dist/umd/popper.min.js"></script>
      <script src="{{ url('stisla/assets/modules/tooltip.js') }}"></script>
      <script src="{{ url('stisla/assets/modules/bootstrap/js/bootstrap.min.js') }}"></script>
      <script src="{{ url('stisla/assets/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
      <script src="{{ url('stisla/assets/modules/moment.min.js') }}"></script>
      <script src="{{ url('stisla/assets/js/stisla.js')}}"></script>
      <!-- JS Libraies -->
      <script src="{{ asset('stisla/assets/modules/datatables/datatables.min.js') }}"></script>
      <script src="{{ asset('stisla/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
      <script src="{{ asset('stisla/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
      <script src="{{ asset('stisla/assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
      <!-- Page Specific JS File -->
      <script src="{{ asset('stisla/assets/js/page/modules-datatables.js') }}"></script>
      <script src="{{ asset('stisla/assets/js/page/modules-sweetalert.js') }}"></script>
      <script src="{{ asset('stisla/assets/js/page/bootstrap-modal.js') }}"></script>
      <!-- JS Libraies -->
      <script src="{{ asset('stisla/assets/modules/sweetalert/sweetalert.min.js') }}"></script>  
      <!-- Template JS File -->
      <script src="{{ asset('stisla/assets/js/scripts.js') }}"></script>
      <script src="{{ asset('stisla/assets/js/custom.js') }}"></script>
      <script>
         function deletedata(id){
           // alert(a);
           let token   = $("meta[name='csrf-token']").attr("content");
           swal({
             title: 'Are you sure?',
             text: 'Once deleted, you will not be able to recover this imaginary file!',
             icon: 'warning',
             buttons: true,
             dangerMode: true,
           })
           .then((willDelete) => {
             if (willDelete) {
               // ajax disini
                 $.ajax({
                   url:'/datamenu/'+id,
                   type :"PUT",
                   cache: false,
                   data : {
                     "id" : id,
                     "_token" : token,
                   },
                   success : function(data){
                     console.log(data)
                   },
                   error : function(error){
                     console.log(error)
                   }
                 });
         
               // hapus data 
             swal('Poof! Your imaginary file has been deleted!', {
               icon: 'success',
             });
             } else {
             swal('Your imaginary file is safe!');
             }
           });
         }
         
         function edit(id){
          $('#datamenu').html('');
          var html ='';
           let token   = $("meta[name='csrf-token']").attr("content");
           $.ajax({
            url : '/datamenu/'+id,
            type :'GET',
            dataType:'json',
            success: function(data){
            //   console.log(data)
              var html = `<form action="#" id="ubahdata">
                <div class="form-group">
                    <label>Nama Menu</label>
                    <input type="hidden" class="form-control" required="" value="${data.id}" id="id">
                    <input type="text" class="form-control" required="" value="${data.nama_menu}" id="namamenu">
                </div>
                <div class="form-group">
                    <label>Icon</label>
                    <input type="text" class="form-control" required="" value="${data.icon}" id="iconmenu">
                </div>
                <div class="form-group">
                    <button class="btn btn-sm btn-primary" onClick="updatedata()">Ubah Data</button>
                </div>
              </form>`
              $('#datamenu').append(html);
              $('#fire-modal-1').modal('show');

            }


           });
                
         }

         function updatedata(){
            let token   = $("meta[name='csrf-token']").attr("content");
            var namamenu = $("#namamenu").val();
            var id = $("#id").val();
            var iconmenu = $("#iconmenu").val();

            $.ajax({
               url : 'datamenu/update',
               type :'POST',
               dataType:'json',
               data:{
                  "_token" : token,
                  "namamenu" :namamenu,
                  "id" : id,
                  "iconmenu" : iconmenu,
               },
               success: function(data){
                  console.log(data);
                  $('#fire-modal-1').modal('hide');

               }
            });   
         }

         




         
         
      </script>
   </body>
</html>