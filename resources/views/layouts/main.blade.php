<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Visi dan Misi</title>
      <!-- Link ke Bootstrap CSS -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" />
      <!-- Link ke Bootstrap Icons CSS -->
      {{-- 
      <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.17.0/font/bootstrap-icons.css" rel="stylesheet" />
      --}}
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css" integrity="sha384-b6lVK+yci+bfDmaY1u0zE8YYJt0TZxLEAFyYSLHId4xoVvsrQu3INevFKo+Xir8e" crossorigin="anonymous" />
      <!-- Option 1: Include in HTML -->
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
      <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" />

      @yield('css')
      <style>
         <!-- CSS Khusus Untuk Navbar Sticky -->
      <style>
         /* Gaya Navbar Sticky */
         .sticky-top {
         position: sticky;
         top: 0;
         z-index: 1000;
         }
         /* Gaya Carousel */
         .carousel-item {
         height: auto;
         }
         /* Gaya Card */
         .custom-card {
         background-color: #0097a7;
         color: #fff;
         }
         /* Gaya Tombol 3D */
         .btn-3d {
         background-color: #0097a7;
         color: #fff;
         text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.2);
         border: none;
         border-radius: 8px;
         transition: transform 0.2s;
         }
         .btn-3d:hover {
         transform: scale(1.05);
         }
         .custom-bg {
         background-color: #0097a7; /* Ganti dengan warna yang Anda inginkan */
         color: #fff; /* Warna teks */
         }
         .kemenkes-bg {
         background-color: #b2bec3; /* Warna biru tua */
         color: white; /* Warna teks putih */
         }
         .accordion-button:not(.collapsed) {
         background-color: #0097a7; /* Warna biru tua ketika terbuka */
         color:white
         }

       /* Responsif untuk Tampilan Mobile */ 
         @media (max-width: 767px) {
         .navbar-nav {
         flex-direction: column;
         text-align: center;
         }
         }
      </style>
   </head>
   <body style="background-color: #f5f5f5; font-family: verdana;">
      <!-- Header Informasi Website -->
      <div class="container-fluid custom-card">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="text-white text-left py-2">
                     <p><img src="https://bpfkmakassar.go.id/images/logo-mobile.png" /></p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Navbar -->
     @include('layouts.sidebar')
      <!-- Slider di Bawah Navbar -->

      <div class="container">
         @yield('konten')         {{-- end konten body --}}
      </div>

      @include('layouts.footer')
      <script
         src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
         integrity="sha512-2rNj2KJ+D8s1ceNasTIex6z4HWyOnEYLVC3FigGOmyQCZc2eBXKgOxQmo3oKLHyfcj53uz4QMsRCWNbLd32Q1g=="
         crossorigin="anonymous"
         referrerpolicy="no-referrer"
         ></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js"></script>

      @yield('js')
   </body>
</html>