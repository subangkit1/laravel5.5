<footer class="bg-white w-100" style="position:none;bottom:0;">
         <div class="container py-4">
            <div class="row">
               <div class="col-md-6">
                  <h6>Footer Content</h6>
                  <p>Tambahkan konten footer Anda di sini.</p>
               </div>
               <div class="col-md-6">
                  <h6>Contact Information</h6>
                  <address>
                     Alamat Anda<br>
                     Kota, Negara<br>
                     Email: email@example.com<br>
                     Telepon: +123456789
                  </address>
               </div>
            </div>
         </div>
      </footer>