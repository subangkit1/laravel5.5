      <!-- Navbar -->
   
      <nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
         <div class="container">
            {{-- <a class="navbar-brand" href="#">Logo</a> --}}
            <!-- Tombol Toggle untuk Tampilan Mobile -->
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <!-- Daftar Menu -->
            <div class="collapse navbar-collapse" id="navbarNav">
            @php
               use App\menu\Menu;
               $menu = Menu::where('status_aktif','=','1')->get();
            @endphp
               <ul class="navbar-nav ml-auto">
                  {{-- static beranda --}}
                  <li class="nav-item">
                     <a class="nav-link" href="#"><i class="bi bi-house-door"></i> Beranda</a>
                  </li> 
                  {{-- submenu --}}
                  @foreach ($menu as $menu)
                  <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bi bi-person"></i>{{ @$menu->nama_menu }}</a>
                     @if (count($menu->submenu) > 0)
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                           @foreach ($menu->submenu as $menu)
                           <a class="dropdown-item" href="{{ @$menu->url_link }}">{{ $menu->namasub_menu}}</a>
                           @endforeach
                        </div>
                     @endif
                  </li>
                  {{-- bawah --}}
               @endforeach 
               {{-- event --}}
               <li class="nav-item">
                     <a class="nav-link" href="#"><i class="bi bi-info-circle"></i> Event</a>
               </li>
               {{-- static menu unduhan--}}
               <li class="nav-item">
                     <a class="nav-link" href="#"><i class="bi bi-download"></i> Unduhan</a>
               </li>
               </ul>
               
               
            </div>
         </div>
      </nav>
      
      <!-- Slider di Bawah Navbar -->