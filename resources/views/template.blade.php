<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Beranda</title>
      <!-- Link ke Bootstrap CSS -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" />
      <!-- Link ke Bootstrap Icons CSS -->
      {{-- 
      <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.17.0/font/bootstrap-icons.css" rel="stylesheet" />
      --}}
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css" integrity="sha384-b6lVK+yci+bfDmaY1u0zE8YYJt0TZxLEAFyYSLHId4xoVvsrQu3INevFKo+Xir8e" crossorigin="anonymous" />
      <!-- Option 1: Include in HTML -->
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
      <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" />
      <style>
         <!-- CSS Khusus Untuk Navbar Sticky -->
      <style>
         /* Gaya Navbar Sticky */
         .sticky-top {
         position: sticky;
         top: 0;
         z-index: 1000;
         }
         /* Gaya Carousel */
         .carousel-item {
         height: auto;
         }
         /* Gaya Card */
         .custom-card {
         background-color: #0097a7;
         color: #fff;
         }
         /* Gaya Tombol 3D */
         .btn-3d {
         background-color: #0097a7;
         color: #fff;
         text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.2);
         border: none;
         border-radius: 8px;
         transition: transform 0.2s;
         }
         .btn-3d:hover {
         transform: scale(1.05);
         }
         .custom-bg {
         background-color: #0097a7; /* Ganti dengan warna yang Anda inginkan */
         color: #fff; /* Warna teks */
         }
         .kemenkes-bg {
         background-color: #b2bec3; /* Warna biru tua */
         color: white; /* Warna teks putih */
         }
         .accordion-button:not(.collapsed) {
         background-color: #0097a7; /* Warna biru tua ketika terbuka */
         color:white
         }

         /* CSS untuk mengatur warna kuning pada radio button */
        .custom-control-input:checked ~ .custom-control-label::before {
            background-color: yellow; /* Warna kuning */
            border-color: yellow; /* Warna border */
        }
         /* Responsif untuk Tampilan Mobile */
         @media (max-width: 767px) {
         .navbar-nav {
         flex-direction: column;
         text-align: center;
         }
         }
      </style>
   </head>
   <body style="background-color: #f5f5f5; font-family: verdana;">
      <!-- Header Informasi Website -->
      <div class="container-fluid custom-card">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="text-white text-left py-2">
                     <p><img src="https://bpfkmakassar.go.id/images/logo-mobile.png" />Selamat datang di Website<strong> BPFAK Surabaya</strong> (Masih dalam Pengembangan)</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
         <div class="container">
            {{-- <a class="navbar-brand" href="#">Logo</a> --}}
            <!-- Tombol Toggle untuk Tampilan Mobile -->
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <!-- Daftar Menu -->
            @include('layouts.sidebar')
            {{-- <div class="collapse navbar-collapse" id="navbarNav">
               <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                     <a class="nav-link" href="#"><i class="bi bi-house-door"></i> Beranda</a>
                  </li>
                  <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bi bi-person"></i> Profil </a>
                     <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                        <a class="dropdown-item" href="#">Visi & Misi</a>
                        <a class="dropdown-item" href="#">Sejarah</a>
                        <a class="dropdown-item" href="#">Struktur Organisasi</a>
                        <a class="dropdown-item" href="#">Kontak</a>
                     </div>
                  </li>
                  <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bi bi-person"></i> Event </a>
                     <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                        <a class="dropdown-item" href="#">Submenu 1</a>
                        <a class="dropdown-item" href="#">Submenu 2</a>
                     </div>
                  </li>
                  <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bi bi-person"></i> Informasi Publik </a>
                     <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                        <a class="dropdown-item" href="#">Submenu 1</a>
                        <a class="dropdown-item" href="#">Submenu 2</a>
                     </div>
                  </li>
                  <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bi bi-person"></i> Unduh </a>
                     <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                        <a class="dropdown-item" href="#">Submenu 1</a>
                        <a class="dropdown-item" href="#">Submenu 2</a>
                     </div>
                  </li>
                  <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bi bi-person"></i> Layanan </a>
                     <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                        <a class="dropdown-item" href="#">Submenu 1</a>
                        <a class="dropdown-item" href="#">Submenu 2</a>
                     </div>
                  </li>
                  <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bi bi-gear"></i> Pengaturan </a>
                     <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                        <a class="dropdown-item" href="#">Submenu 1</a>
                        <a class="dropdown-item" href="#">Submenu 2</a>
                     </div>
                  </li>
               </ul>
            </div> --}}
         </div>
      </nav>
      <!-- Slider di Bawah Navbar -->
      <div id="carouselExample" class="carousel slide" data-bs-ride="carousel">
         <div class="carousel-inner">
            <div class="carousel-item active">
               <img src="https://seosecret.id/placeholder/1024x300/d5d5d5/584959" class="d-block w-100" alt="Gambar 1" />
               <div class="carousel-caption d-none d-md-block">
                  <h5>First slide label</h5>
                  <p>Some representative placeholder content for the first slide.</p>
               </div>
            </div>
            <div class="carousel-item">
               <img src="https://seosecret.id/placeholder/1024x300/d5d5d5/584959" class="d-block w-100" alt="Gambar 2" />
            </div>
            <!-- Tambahkan lebih banyak slide di sini sesuai kebutuhan Anda -->
         </div>
         <a class="carousel-control-prev" href="#carouselExample" role="button" data-bs-slide="prev">
         <span class="carousel-control-prev-icon" aria-hidden="true"></span>
         <span class="visually-hidden">Sebelumnya</span>
         </a>
         <a class="carousel-control-next" href="#carouselExample" role="button" data-bs-slide="next">
         <span class="carousel-control-next-icon" aria-hidden="true"></span>
         <span class="visually-hidden">Selanjutnya</span>
         </a>
      </div>
      <!-- Informasi dengan 2 Card -->
      <div class="container">
         <div class="row mt-3 mb-3">
            <div class="col-md-6">
               <div class="card" style="background-color: #8a1538; color: white;">
                  <div class="card-body">
                     <h5 class="card-title">SIMLPK Online Client</h5>
                     <p class="card-text">SIMLPK Pengajuan Online Proses pengajuan permohonan pengujian/kalibrasi melalui aplikasi online..</p>
                     <button class="btn btn-3d">Ajukan Permohonan</button>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="card" style="background-color: #003955; color: white;">
                  <div class="card-body">
                     <h5 class="card-title">Kuisioner Pelanggan</h5>
                     <p class="card-text">Dalam rangka meningkatkan mutu layanan di Balai Pengamanan Fasilitas Kesehatan (BPFK) Surabaya</p>
                     <button class="btn btn-3d">Pilih</button>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            {{-- 
            <p>Berita</p>
            --}}
         </div>
         {{-- row card berita--}}
         <div class="row mt-2 mb-4">
            <div class="col-md-3">
               <div class="card" style="background-color: #f5f5f5; border: none; padding: 20px;">
                  <img src="https://seosecret.id/placeholder/200x200/d5d5d5/584959/PITA" class="card-img-top" style="border-radius: 1em;" alt="..." />
                  <div class="card-body">
                     <h6 class="card-title">Kaji Ulang Manajemen BPFK Makassar Tahun 2022</h6>
                     <p class="card-text" style="font-size: 8pt;">02 September 2023</p>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="card" style="background-color: #f5f5f5; border: none; padding: 20px;">
                  <img src="https://seosecret.id/placeholder/200x200/d5d5d5/584959/NOE" class="card-img-top" style="border-radius: 1em;" alt="..." />
                  <div class="card-body">
                     <h6 class="card-title">BAPETEN RI ANUGERAHI BPFK MAKASSAR SEBAGAI INSTANS…</h6>
                     <p class="card-text" style="font-size: 8pt;">02 September 2023</p>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="card" style="background-color: #f5f5f5; border: none; padding: 20px;">
                  <img src="https://seosecret.id/placeholder/200x200/d5d5d5/584959/PITA" class="card-img-top" style="border-radius: 1em;" alt="..." />
                  <div class="card-body">
                     <h6 class="card-title">PERTEMUAN MUTU LAYANAN P/K BPFK MKS DAN PENGUATAN …</h6>
                     <p class="card-text" style="font-size: 8pt;">02 September 2023</p>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="card" style="background-color: #f5f5f5; border: none; padding: 20px;">
                  <img src="https://seosecret.id/placeholder/200x200/d5d5d5/584959/NOE" class="card-img-top" style="border-radius: 1em;" alt="..." />
                  <div class="card-body">
                     <h6 class="card-title">Pertemuan Sosialisasi PNPB dan Mutu Layanan Penguj…</h6>
                     <p class="card-text" style="font-size: 8pt;">02 September 2023</p>
                  </div>
               </div>
            </div>
            <span class="text-end">
               <p>Lainnya</p>
            </span>
         </div>
         {{-- penjadwalan --}}
         <div class="row mt-5">
            <div class="col-md-8">
               <div class="row">
                  <div class="col-md-12">
                     <h4>Jadwal Kunjungan Pengujian Dan /Atau Kalibrasi Alat Kesehatan</h4>
                     Rencana Jadwal Kunjungan Pengujian Dan /Atau Kalibrasi Alat Kesehatan
                  </div>
               </div>
               <div class="row mt-5">
                  {{-- looping disini --}}
                  <div class="col-md-4">
                     <p style="font-size: 10pt">19 Sep 2023 - 29 Sep 2023</p>
                  </div>
                  <div class="col-md-8">
                     <p style="font-size: 10pt">PUSKESMAS LIUKANG TANGAYA ; PUSKESMAS SARAPPO & PUSKESMAS LIUKANG TUPABBIRING KAB. PANGKEP</p>
                  </div>
                  <hr />
                  <div class="col-md-4">
                     <p style="font-size: 10pt">19 Sep 2023 - 29 Sep 2023</p>
                  </div>
                  <div class="col-md-8">
                     <p style="font-size: 10pt">PUSKESMAS LIUKANG TANGAYA ; PUSKESMAS SARAPPO & PUSKESMAS LIUKANG TUPABBIRING KAB. PANGKEP</p>
                  </div>
                  <hr />
                  <div class="col-md-4">
                     <p style="font-size: 10pt">19 Sep 2023 - 29 Sep 2023</p>
                  </div>
                  <div class="col-md-8">
                     <p style="font-size: 10pt">DINAS KESEHATAN KAB. SERAM BAGIAN BARAT</p>
                  </div>
                  <hr />
                  <div class="col-md-4">
                     <p style="font-size: 10pt">19 Sep 2023 - 29 Sep 2023</p>
                  </div>
                  <div class="col-md-8">
                     <p style="font-size: 10pt">RSUD H PADJONGA DG. NGALLE TAKALAR</p>
                  </div>
                  <hr />
                  {{-- end looping disini --}}
               </div>
            </div>
            <div class="col-md-4">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-header" style="background-color: #0097a7;color:white">
                           <i class="bi bi-info-circle"></i> Informasi  
                        </div>
                        <div class="card-body">
                           <ul class="list-group">
                              <li class="list-group-item">An item</li>
                              <li class="list-group-item">A second item</li>
                              <li class="list-group-item">A third item</li>
                              <li class="list-group-item">A fourth item</li>
                              <li class="list-group-item">And a fifth one</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         {{-- end penjadwalan --}}
      </div>
      {{-- container kedua --}}
      <div class="container-fluid custom-bg">
         <div class="container">
            <div class="row" >
               <div class="col-md-12 mt-5 mb-5">
                  <div class="row text-center">
                     <h5>LAYANAN BPAFK SURABAYA</h5>
                  </div>
                  <div class="row text-center" style="margin-top:20px;margin-button:20px">
                     <div class="col-md-6">
                        <div class="card" style="background-color:none; border-color:none;">
                          <img class="card-img-top" src="https://bpfkmakassar.go.id/images/2022/02/16/lab-kal-2.jpg" alt="Title">
                        </div>
                        {{-- <img src="https://bpfkmakassar.go.id/images/2022/02/16/lab-kal-2.jpg" class="rounded mx-auto d-block" style="width:500px;"> --}}
                     </div>
                     <div class="col-md-6">

                        <div class="card" style="background-color:none; border-color:none;">
                          <img class="card-img-top" src="https://bpfkmakassar.go.id/images/2022/02/16/lab-kal-2.jpg" alt="Title">
                        </div>
                        
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="container-fluid mt-5" >
         <div class="container" >
            <div class="row">
               <div class="col-md-4">
                  <div class="row">
                     <div class="col-md-12">
                        <h6>Mengapa Harus Kalibrasi di kami?</h6>
                     </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                        <img src="https://dummyimage.com/100x100/000/ff7675" class="rounded" alt="">
                       <img src="https://dummyimage.com/100x100/000/ff7675" class="rounded" alt="">
                    </div>
                     
                  </div>

                  <div class="row mt-2">
                    <div class="col-sm-12">
                        <img src="https://dummyimage.com/100x100/ff26ff/ffffff" class="rounded" alt="">
                        <img src="https://dummyimage.com/100x100/000/ff7675" class="rounded" alt="">
                    </div>
                     
                  </div>
               </div>
               <div class="col-md-8">
                  <div class="row">
                     <div class="col-md-12">
                        {{-- open --}}
                        <div class="accordion" id="accordionExample">
                           <!-- Panel 1 -->
                           <div class="accordion-item">
                              <h6 class="accordion-header kemenkes-bg" id="headingOne">
                                 <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                 UNDANG-UNDANG RI NOMOR 36 TAHUN 2009 TENTANG KESEHATAN
                                 </button>
                              </h6>
                              <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                 <div class="accordion-body">
                                    <p>Pasal 104</p>
                                    (1) Pengamanan sediaan farmasi dan alat kesehatan diselenggarakan untuk melindungi masyarakat dari bahaya yang disebabkan oleh penggunaan sediaan farmasi dan alat kesehatan yang tidak memenuhi persyaratan mutu dan/atau keamanan dan/atau khasiat/kemanfaatan.
                                 </div>
                              </div>
                           </div>
                           <!-- Panel 2 -->
                           <div class="accordion-item">
                              <h2 class="accordion-header kemenkes-bg" id="headingTwo">
                                 <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <p style="color:white">UNDANG-UNDANG RI NOMOR 44 TAHUN 2009 TENTANG RUMAH SAKIT</p>
                                 </button>
                              </h2>
                              <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                 <div class="accordion-body">
                                    <p>Bagian Ketujuh (Peralatan)
                                    Pasal 16</p>
                                    (1) Persyaratan peralatan sebagaimana dimaksud dalam Pasal 7 ayat (1) meliputi peralatan medis dan nonmedis harus memenuhi standar pelayanan, persyaratan mutu, keamanan, keselamatan dan laik pakai.
                                    (2) Peralatan medis sebagaimana dimaksud pada ayat (1) harus diuji dan dikalibrasi secara berkala oleh Balai Pengujian Fasilitas Kesehatan dan/atau institusi pengujian fasilitas kesehatan yang berwenang.
                                    (3) Peralatan yang menggunakan sinar pengion harus memenuhi ketentuan dan harus diawasi oleh lembaga yang berwenang.
                                 </div>
                              </div>
                           </div>
                           <!-- Panel 3 -->
                           <div class="accordion-item">
                              <h2 class="accordion-header kemenkes-bg" id="headingThree">
                                 <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                 PERATURAN MENTERI KESEHATAN RI NOMOR: 363/MENKES/PER/IV/1998 TENTANG PENGUJIAN DAN KALIBRASI ALAT KESEHATAN PADA SARANA PELAYANAN KESEHATAN
                                 </button>
                              </h2>
                              <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                 <div class="accordion-body">
                                    <p>Pasal 2</p>
                                    (1) Setiap alat kesehatan wajib dilakukan pengujian dan/atau kalibrasi untuk menjamin kebenaran nilai keluaran atau kinerja, keselamatan dan kemananan pemakaian.
                                    (2) Pengujian dan/atau kalibrasi serta pengukuran paparan/kebocoran radiasi fasilitas kesehatan dilakukan secara berkala, sekurang-kurangnya satu kali dalam satu tahun.
                                 </div>
                              </div>
                           </div>
                        </div>
                        {{-- end --}}
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>


      {{-- keunggulan  --}}
      <div class="container-fluid mt-5" style="background-color: #003955">
        <div class="container py-5">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            KEUNGGULAN
                        </div>
                        <div class="card-body">
                            {{-- <h4 class="card-title">Title</h4> --}}
                            <p class="card-text">BPAFK Surabaya senantiasa berupaya memberikan pelayanan yang bermutu dan cepat untuk menjamin keamanan dan keselamatan kerja peralatan kesehatan.</p>
                        </div>  
                    </div>

                </div>


                <div class="col-md-4">

                    <div class="card">
                        <div class="card-header">
                            LABORATORIUM TERAKREDITASI
                        </div>
                        <div class="card-body">
                            {{-- <h4 class="card-title">Title</h4> --}}
                            <img src="" alt="">
                        </div>  
                    </div>

                </div>
                <div class="col-md-4">

                    <div class="card">
                        <div class="card-header">
                            SDM YANG BERSERTIFIKAT / SERTIFIKASI
                        </div>
                        <div class="card-body">
                            {{-- <h4 class="card-title">Title</h4> --}}
                            <img src="" alt="">
                        </div>  
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4"></div>
                <div class="col-md-4"></div>
            </div>
        </div>
      </div>


      {{-- end keuanggulan --}}
      {{-- footer --}}
      <footer class="bg-white">
        <div class="container py-4">
            <div class="row">
                <div class="col-md-3">
                    <h6>Footer Content</h6>
                    <p>Tambahkan konten footer Anda di sini.</p>
                </div>
                <div class="col-md-3">
                    <h6>Contact Information</h6>
                    <address>
                        Alamat Anda<br>
                        Kota, Negara<br>
                        Email: email@example.com<br>
                        Telepon: +123456789
                    </address>
                </div>

                <div class="col-md-3">
                  <div class="row">
                     <h5>TAUTAN</h5>
                     <div class="col-md-6">
                        
                     </div>
                     <div class="col-md-6">
                        
                     </div>
                  </div>
                </div>

                <div class="col-md-3">
                  <h4>POLING</h4>
                  <p>Bagaimana pendapat anda tentang pelayanan kami ?</p>
                  <div class="custom-control custom-radio">
            <input type="radio" id="radio1" name="customRadio" class="custom-control-input">
            <label class="custom-control-label" for="radio1">Pilihan 1</label>
        </div>
        <div class="custom-control custom-radio">
            <input type="radio" id="radio2" name="customRadio" class="custom-control-input">
            <label class="custom-control-label" for="radio2">Pilihan 2</label>
        </div>
                  

                </div>


            </div>
        </div>
    </footer>
      {{-- end footer --}}


      <!-- Tiga Kolom dengan Pewarnaan Kemenkes -->
      <!-- Link ke Bootstrap JavaScript (Popper.js dan Bootstrap.js) -->
      <script
         src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
         integrity="sha512-2rNj2KJ+D8s1ceNasTIex6z4HWyOnEYLVC3FigGOmyQCZc2eBXKgOxQmo3oKLHyfcj53uz4QMsRCWNbLd32Q1g=="
         crossorigin="anonymous"
         referrerpolicy="no-referrer"
         ></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js"></script>
   </body>
</html>